package serviceRessource;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;


import entities.Personne;
import services.ClientService;
import services.ClientServiceLocale;

@Path("personnePI")
public class Implementation {
	@EJB
	ClientServiceLocale clientService ;

	public Implementation () {
	}
	
	@Path("/GetAll")
	@GET
	@Produces(MediaType.APPLICATION_XML) // le retour
	public List<Personne> getAllPersonnes() {	
		return clientService.listerPersonnes() ; 
	}
	 
	@Path("/addPerson")
	@POST
	@Consumes(MediaType.APPLICATION_XML) // l'entree
	@Produces(MediaType.TEXT_PLAIN) // le retour	
	public String addPersonne(Personne P) {
		if (clientService.ajouterPersonne(P)) {  
			return "ajout avec succes " ;  /// la reponse 
		}else {
			return "erreur l'ors de l'ajout" ;/// la reponse 
		}
	}
	
	
	
/*	
	///c 'est juste mais ghir ena j'ai fais les commentaire
	@PUT
	@Path("/{identif}")
	@Consumes(MediaType.APPLICATION_XML) // l'entree
	@Produces(MediaType.TEXT_PLAIN) // le retour (sortie)	
	public String updateEmploye(@PathParam(value="identif") String id , employe E) {
		for (int i=0;i<list.size();i++) {
			if (list.get(i).getCin().equals(id)) {
				list.get(i).setNom(E.getNom());
				list.get(i).setPrenom(E.getPrenom());
				return "Update Successful" ; 
			}
		}
		return "Update UNNNNSuccessful" ;
	}
	*/
	/*@GET
	@Path("/{identif}")
	//@Consumes(MediaType.APPLICATION_XML) // l'entree
	@Produces(MediaType.APPLICATION_XML) // le retour (sortie)
	public employe searchEmploye(@PathParam(value="identif") String id) {
		for (int i=0;i<list.size();i++) {
			if (list.get(i).getCin().equals(id)) {
				return list.get(i) ; 
			}
		}
		return null ;      
	}
	
	@POST
	@Path("/{identif}")
	//@Consumes(MediaType.APPLICATION_XML) // l'entree
	@Produces(MediaType.TEXT_PLAIN) // le retour (sortie)
	public String deleteEmploye(@PathParam(value="identif") String id) {
		int e = -1 ; 
		for (int i=0;i<list.size();i++) {
			if (list.get(i).getCin().equals(id)) {
				e=i;
			}
		}
		if (e==-1) {
			return "false" ;
		}else {
			list.remove(e) ; 
			return "true" ; 
		}
			

	}
	/// display and search with get 
	///C EST JUSTE POUR UTILISER UN QUERY PARAM
	///@QueryParam (ya3ni les annotations )kifech l client externe bech ya7ki m3aya 
	@PUT 
	//@Path("/{identif}")
	@Consumes(MediaType.APPLICATION_XML) // l'entree
	@Produces(MediaType.TEXT_PLAIN) // le retour (sortie)	
	public String updateEmployeQUERY_PARAM(@QueryParam(value="identif") String id , employe E) {
		for (int i=0;i<list.size();i++) {
			if (list.get(i).getCin().equals(id)) {
				list.get(i).setNom(E.getNom());
				list.get(i).setPrenom(E.getPrenom());
				return "Update Successful" ; 
			}
		}
		return "Update UNNNNSuccessful" ;
	} */
}
