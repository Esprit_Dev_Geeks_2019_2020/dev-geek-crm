package serviceRessource;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import entities.Question;
import interfaces.IQuestionService;

@Path("questions")
@Stateless
public class QuestionRessources implements Serializable {

	@EJB
	IQuestionService QS;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getQuestion(@QueryParam(value = "idAnswer") String idAnswer) {
		if (idAnswer == null) {
			if(QS.findAllQuestion() == null) {
				return Response.status(Status.BAD_REQUEST).build();
			}
		else if (QS.findAllQuestion().size() == 0) {
				return Response.status(Status.NOT_FOUND).entity("vide").build();
			} else
				return Response.status(Status.FOUND).entity(QS.findAllQuestion()).build();

		}
		else if (idAnswer != null) {
			if (QS.findQuestion(Integer.parseInt(idAnswer)) == null) {
				return Response.status(Status.NOT_FOUND).entity("wrong id Or Question have been removed").build();
			} else
				return Response.status(Status.FOUND).entity(QS.findQuestion(Integer.parseInt(idAnswer))).build();
		}
		
			return Response.status(Status.BAD_REQUEST).build();

	}
	@POST
	@Consumes(MediaType.APPLICATION_JSON) // l'entree
	@Produces(MediaType.APPLICATION_JSON) // le retour	
	public Response addQuestion(Question e) {
		if (QS.addQuestion(e) !=0) {  
			 return Response.ok(QS.addQuestion(e),MediaType.APPLICATION_JSON).build();
		}else {
			return Response.status(Status.BAD_REQUEST).entity("unsuccessful").build();
		}
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateQuestion(Question e) {
		int id=e.getIdQuestion();
		
		if (QS.findQuestion(e.getIdQuestion()) != null) {  
			QS.QuestionUpdate(e);
			 return Response.ok("sucessful",MediaType.APPLICATION_JSON).build();
		}else {
			return Response.status(Status.BAD_REQUEST).entity("unsuccessful").build();
		}	
	}

}
