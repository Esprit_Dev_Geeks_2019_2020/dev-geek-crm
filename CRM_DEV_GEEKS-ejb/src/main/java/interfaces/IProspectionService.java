package interfaces;

import java.util.List;

import javax.ejb.Local;

import entities.Prospection;


@Local
public interface IProspectionService {
	public int addProspection(Prospection emp);
	 public void removeProspection(int id);
	public void ProspectionUpdate(Prospection emp);
	public Prospection findProspection(int id);
	
	public List<Prospection> findAllProspection();
}
