package interfaces;

import java.util.List;

import javax.ejb.Local;

import entities.Question;

@Local
public interface IQuestionService {

	public int addQuestion(Question Q);
	 public void removeQuestion(int id);
	public void QuestionUpdate(Question emp);
	public Question findQuestion(int id);
	
	public List<Question> findAllQuestion();
}
