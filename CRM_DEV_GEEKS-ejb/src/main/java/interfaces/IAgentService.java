package interfaces;

import java.util.List;

import javax.ejb.Local;

import entities.Agent;
@Local
public interface IAgentService {
	public int addAgent(Agent Q);
	 public void removeAgent(int id);
	public void AgentUpdate(Agent emp);
	public Agent findAgent(int id);
	public List<Agent> findAllAgent();
}
