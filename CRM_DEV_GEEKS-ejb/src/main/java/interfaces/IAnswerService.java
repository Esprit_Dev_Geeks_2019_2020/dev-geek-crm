package interfaces;

import java.util.List;

import javax.ejb.Local;

import entities.Answer;
@Local
public interface IAnswerService {
	public int addAnswer(Answer Q);
	 public void removeAnswer(int id);
	public void AnswerUpdate(Answer emp);
	public Answer findAnswer(int id);
	public List<Answer> findAllAnswer();
}
