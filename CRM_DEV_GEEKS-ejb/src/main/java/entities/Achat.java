package entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.Date;


@XmlRootElement
@Entity
public class Achat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idCommande;

	@Temporal(TemporalType.DATE)
	private Date dateCommande;

	
	private int idPersonne;

	private String listeproduit;

	
	
	
	@ManyToOne
	private Personne personne  ;
	
	
	
	
	
	
	
	
	public Achat() {
	}

	public int getIdCommande() {
		return this.idCommande;
	}

	public void setIdCommande(int idCommande) {
		this.idCommande = idCommande;
	}

	public Date getDateCommande() {
		return this.dateCommande;
	}

	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}

	public int getIdPersonne() {
		return this.idPersonne;
	}

	public void setIdPersonne(int idPersonne) {
		this.idPersonne = idPersonne;
	}

	public String getListeproduit() {
		return this.listeproduit;
	}

	public void setListeproduit(String listeproduit) {
		this.listeproduit = listeproduit;
	}

}