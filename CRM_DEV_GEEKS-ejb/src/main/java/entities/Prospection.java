package entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import javax.persistence.*;
@Entity
@Table(name = "prospection")
public class Prospection {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int idProspection;
	@Column
	@Temporal(TemporalType.DATE)
	Date startDate;
	@Column
	@Temporal(TemporalType.DATE)
	Date endDate;
	@Column
	Boolean isReached;
	@ManyToMany
    @JoinTable(name="FieldProspection",
    		   joinColumns=@JoinColumn(name="idProspection"),
    		   inverseJoinColumns=@JoinColumn(name="idAgent"))
   
	 private List<Agent> agent = new ArrayList<Agent>();
	public int getIdProspection() {
		return idProspection;
	}
	public void setIdProspection(int idProspection) {
		this.idProspection = idProspection;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public List<Agent> getAgent() {
		return agent;
	}
	public void setAgent(List<Agent> agent) {
		this.agent = agent;
	}
	public Prospection(Date startDate, Date endDate, Boolean isReached) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.isReached = isReached;
	}
	public Boolean getIsReached() {
		return isReached;
	}
	public void setIsReached(Boolean isReached) {
		this.isReached = isReached;
	}
	
	
}
