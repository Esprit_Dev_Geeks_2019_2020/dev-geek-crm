package entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "QtePerStore")
@IdClass(QtePerStore.class)
public class QtePerStore  implements  Serializable{
	@Id
	@ManyToOne
	@JoinColumn (name = "idProduit" , referencedColumnName = "IdProduit")
	Product product ; 
	@Id
	@ManyToOne
	@JoinColumn (name = "idStore" , referencedColumnName = "idStore")
	Store store ; 
	
	int Qte ; 
	Boolean Avialable ;
	
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	

	public Store getStore() {
		return store;
	}
	public void setStore(Store store) {
		this.store = store;
	}
	public int getQte() {
		return Qte;
	}
	public void setQte(int qte) {
		Qte = qte;
	}
	public Boolean getAvialable() {
		return Avialable;
	}
	public void setAvialable(Boolean avialable) {
		Avialable = avialable;
	}
	
	

}
