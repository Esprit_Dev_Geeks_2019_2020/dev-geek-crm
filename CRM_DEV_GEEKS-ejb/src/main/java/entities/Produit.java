package entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@Entity
public class Produit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idProduit;	
	
	@OneToMany(mappedBy="produit",cascade= {CascadeType.ALL})
	private List<Commentaire> commentairesproduits ;
	
	@OneToMany(mappedBy="produit",cascade= {CascadeType.ALL})
	private List<Note> notesproduits ;	
	
	public Produit() {
	}

	public int getIdProduit() {
		return this.idProduit;
	}

	public void setIdProduit(int idProduit) {
		this.idProduit = idProduit;
	}

	public List<Commentaire> getCommentairesproduits() {
		return commentairesproduits;
	}

	public void setCommentairesproduits(List<Commentaire> commentairesproduits) {
		this.commentairesproduits = commentairesproduits;
	}

	public List<Note> getNotesproduits() {
		return notesproduits;
	}

	public void setNotesproduits(List<Note> notesproduits) {
		this.notesproduits = notesproduits;
	}
	
	
}