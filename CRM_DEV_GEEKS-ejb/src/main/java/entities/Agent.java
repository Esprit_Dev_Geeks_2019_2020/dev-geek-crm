package entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
@Entity
@Table(name="agent")
public class Agent {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	int idAgent;
	@Column
	String firstname;
	@Column 
	String lastname;
	@Column
	String adress;
	@Column
	String cin;
	@Column
	int indicator;
	@Column
	@Enumerated
	AgentType agentType;
	 @ManyToMany(mappedBy = "agent")
	List<Prospection>Prospections=new ArrayList<Prospection>() ;
	 @Column
	 Boolean availability;
	public int getIdAgent() {
		return idAgent;
	}
	public void setIdAgent(int idAgent) {
		this.idAgent = idAgent;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getCin() {
		return cin;
	}
	public void setCin(String cin) {
		this.cin = cin;
	}
	public int getIndicator() {
		return indicator;
	}
	public void setIndicator(int indicator) {
		this.indicator = indicator;
	}
	public AgentType getAgentType() {
		return agentType;
	}
	public void setAgentType(AgentType agentType) {
		this.agentType = agentType;
	}
	public List<Prospection> getProspections() {
		return Prospections;
	}
	public void setProspections(List<Prospection> prospections) {
		Prospections = prospections;
	}
	public Boolean getAvailability() {
		return availability;
	}
	public void setAvailability(Boolean availability) {
		this.availability = availability;
	}
	public Agent(String firstname, String lastname, String adress, String cin, int indicator, AgentType agentType,
			Boolean availability) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.adress = adress;
		this.cin = cin;
		this.indicator = indicator;
		this.agentType = agentType;
		this.availability = availability;
	}
	public Agent() {
		super();
	}
	 
}
