package entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity(name="answer")
@Table(name="answer")
public class Answer {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	int idAnswer;
	
	@ManyToOne
	@JoinColumn(name = "idUser")
	Personne Personne;
	
	@ManyToOne
	@JoinColumn(name = "idQuestion")
	Question question;
	
	@Temporal(TemporalType.DATE)
	Date dateR;
	
	@Column 
	String text;
	@Column
	int score;
	@Column
	Boolean bestAnswer;
	@Column
	Boolean approvedAnswer;
	public int getIdAnswer() {
		return idAnswer;
	}
	public void setIdAnswer(int idAnswer) {
		this.idAnswer = idAnswer;
	}
	public Personne getUser() {
		return Personne;
	}
	public void setUser(Personne personne) {
		Personne = personne;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public Date getDateR() {
		return dateR;
	}
	public void setDateR(Date dateR) {
		this.dateR = dateR;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public Boolean getBestAnswer() {
		return bestAnswer;
	}
	public void setBestAnswer(Boolean bestAnswer) {
		this.bestAnswer = bestAnswer;
	}
	public Answer(entities.Personne personne, Question question, Date dateR, String text, int score, Boolean bestAnswer) {
		super();
		Personne = personne;
		this.question = question;
		this.dateR = dateR;
		this.text = text;
		this.score = score;
		this.bestAnswer = bestAnswer;
	}
	public Answer() {
		super();
	}
	
}
