package entities;

import java.io.Serializable;
import java.sql.Array;
import java.sql.Blob;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Product")
public class Product implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int IdProduit ; 
	
	@Column(name="Nom")
	String Nom ; 
	
	@Column(name="Description")
	String Description ; 
	
	@Column(name="Color")
	String Color;
	
	@Column(name="rating")
	int rating ; 
	
	@Column(name="Stock")
	int Stock ; 
	
	@Column(name="price")
	double price ; 
	
	@Column(name="img")
	Blob img ;

	@OneToMany(mappedBy="product")
	private List<QtePerStore> Qtes ;
	
	@ManyToOne
	private Categorie categorie ;
	
	
	public Categorie getCategorie() {
		return this.categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	@ManyToOne
	private Marque marque ;
	
	
	public Marque getMarque() {
		return marque ;
	}
	
	public void setMarque(Marque marque) {
		this.marque = marque;
	}

	
	public List<QtePerStore> getQtes(){
		return Qtes;	
	}
	
	public void setQtes(List<QtePerStore> qtes) {
		Qtes = qtes;
	}

	public int getIdProduit() {
		return IdProduit;
	}

	public void setIdProduit(int idProduit) {
		IdProduit = idProduit;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getColor() {
		return Color;
	}

	public void setColor(String color) {
		Color = color;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public int getStock() {
		return Stock;
	}

	public void setStock(int stock) {
		Stock = stock;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Blob getImg() {
		return img;
	}

	public void setImg(Blob img) {
		this.img = img;
	} 
	
	
	
}
