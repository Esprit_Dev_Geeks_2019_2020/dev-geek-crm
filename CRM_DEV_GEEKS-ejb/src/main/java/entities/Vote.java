package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
@Entity(name="vote")
@Table(name="vote")
public class Vote {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	int idVote;
	@ManyToOne
	@JoinColumn(name = "idUser")
	Personne Personne;
	
	@ManyToOne
	@JoinColumn(name = "idAnswer")
	Answer idAnswer;
}
