package entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name ="agents")
public class AgentOperator implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int idAgent ; 
	
	@Column(name = "Nom")
	String Nom ; 
	
	@Column(name = "Prenom")
	String Prenom ; 
	
	@Column(name = "Cin")
	int Cin ; 
	
	@Column(name = "username")
	String username ; 
	
	@Column(name = "password")
	String password ; 
	
	@Column(name = "Mail")
	String Mail ;
	
	@ManyToOne
	private Store store ;
	
	
	public Store getStore() {
		return store ;
	}

	public int getIdAgent() {
		return idAgent;
	}

	public void setIdAgent(int idAgent) {
		this.idAgent = idAgent;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public String getPrenom() {
		return Prenom;
	}

	public void setPrenom(String prenom) {
		Prenom = prenom;
	}

	public int getCin() {
		return Cin;
	}

	public void setCin(int cin) {
		Cin = cin;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMail() {
		return Mail;
	}

	public void setMail(String mail) {
		Mail = mail;
	} 


}
