package entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "Question")
@Table(name = "Question")
public class Question implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int idQuestion;
	@Column
	String intitule;
	@Column
	Boolean isSolved;
	@Column
	@Temporal(TemporalType.DATE)
	Date dateQ;

	@ManyToOne
	@JoinColumn(name = "idUser")
	Personne Personne;

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public Boolean getIsSolved() {
		return isSolved;
	}

	public void setIsSolved(Boolean isSolved) {
		this.isSolved = isSolved;
	}

	public Date getDateQ() {
		return dateQ;
	}

	public void setDateQ(Date dateQ) {
		this.dateQ = dateQ;
	}

	public Personne getUser() {
		return Personne;
	}

	public void setUser(Personne personne) {
		Personne = personne;
	}

	public int getIdQuestion() {
		return idQuestion;
	}

	public void setIdQuestion(int idQuestion) {
		this.idQuestion = idQuestion;
	}

	public Question(String intitule, Boolean isSolved, Date dateQ, entities.Personne personne) {
		super();
		this.intitule = intitule;
		this.isSolved = isSolved;
		this.dateQ = dateQ;
		Personne = personne;
	}

	public Question() {
		super();
	}
	
}
