package entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="Categorie")
public class Categorie implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int idCategorie ;
	
	@Column(name="Nom")
	String nom ; 
	
	@Column (name="Description")
	String Desc ;

	public int getId() {
		return idCategorie;
	}

	public void setId(int id) {
		this.idCategorie = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDesc() {
		return Desc;
	}

	public void setDesc(String desc) {
		Desc = desc;
	}
	
	
	
	
}
