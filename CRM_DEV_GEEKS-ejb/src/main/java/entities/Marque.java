package entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Marque")
public class Marque implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int idMarque ; 
	
	@Column(name="Nom")
	String Nom;
	
	@Column(name="Adresse")
	String Adresse ;

	public int getId() {
		return idMarque;
	}

	public void setId(int id) {
		this.idMarque = id;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public String getAdresse() {
		return Adresse;
	}

	public void setAdresse(String adresse) {
		Adresse = adresse;
	} 

	
	
}
