package entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@Entity
public class Personne implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idPersonne;

	private int actif;

	private String certif;

	private String email;

	private String image;

	private String libellecompany;

	private int nbremp;

	private String nom;

	private String numtel;

	private String password;

	private String prenom;

	@Enumerated(EnumType.STRING)
	private Role role; //client / prospect  
	@Enumerated(EnumType.STRING)
	private Type type;//physque / morale
	
	
	
	
	
	
	@OneToMany(mappedBy="personne",cascade= {CascadeType.ALL})
	private List<Commentaire> commentairespersonne ; 	
	
	@OneToMany(mappedBy="personne",cascade= {CascadeType.ALL})
	private List<Note> notespersonne ; 
	
	@OneToMany(mappedBy="personne",cascade= {CascadeType.ALL})
	private List<Achat> achatspersonne ; 
		
	
	public Personne() {
	}
	public int getIdPersonne() {
		return this.idPersonne;
	}

	public void setIdPersonne(int idPersonne) {
		this.idPersonne = idPersonne;
	}

	public int getActif() {
		return this.actif;
	}

	public void setActif(int actif) {
		this.actif = actif;
	}

	public String getCertif() {
		return this.certif;
	}

	public void setCertif(String certif) {
		this.certif = certif;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getLibellecompany() {
		return this.libellecompany;
	}

	public void setLibellecompany(String libellecompany) {
		this.libellecompany = libellecompany;
	}

	public int getNbremp() {
		return this.nbremp;
	}

	public void setNbremp(int nbremp) {
		this.nbremp = nbremp;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNumtel() {
		return this.numtel;
	}

	public void setNumtel(String numtel) {
		this.numtel = numtel;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
	public List<Commentaire> getCommentairespersonne() {
		return commentairespersonne;
	}
	public void setCommentairespersonne(List<Commentaire> commentairespersonne) {
		this.commentairespersonne = commentairespersonne;
	}
	public List<Note> getNotespersonne() {
		return notespersonne;
	}
	public void setNotespersonne(List<Note> notespersonne) {
		this.notespersonne = notespersonne;
	}
	public List<Achat> getAchatspersonne() {
		return achatspersonne;
	}
	public void setAchatspersonne(List<Achat> achatspersonne) {
		this.achatspersonne = achatspersonne;
	}

	

}