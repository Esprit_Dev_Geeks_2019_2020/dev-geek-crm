package entities;

import java.sql.Time;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Strore")
public class Store {

	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	int idStore;
	
	@Column(name = "Nom")
	String Nom;
	
	@Column(name = "Adresse")
	String Adresse ; 
	
	@Column(name="Tel")
	int Tel ; 
	
	@OneToMany(mappedBy="store")
	private List<AgentOperator> Agents ; 
	
	
	public List<AgentOperator> getAgents(){
		return Agents ;
	}
	
	
	public int getIdStore() {
		return idStore;
	}

	public void setIdStore(int idStore) {
		this.idStore = idStore;
	}

	public void setAgents(List<AgentOperator> agents) {
		Agents = agents;
	}


	@Column(name ="Heure_ouverture")
	Time heure_ouverture ; 
	
	@Column(name ="Heure_fermeture")
	Time Heure_fermeture ;

	@OneToMany(targetEntity = QtePerStore.class , mappedBy="store" ,fetch = FetchType.EAGER)
	private List<QtePerStore> Qtes ;
	
	//@ManyToMany(mappedBy="stores")
	//List<Promotion> promotionStores ;
	
	//public List<Promotion> getPromotions() {
	//	return promotionStores;
//	}


	// public void setPromotions(List<Promotion> promotionStores) {
	//	this.promotionStores = promotionStores;
	//}


	public List<QtePerStore> getQtes() {
		return Qtes;
	}


	public void setQtes(List<QtePerStore> qtes) {
		Qtes = qtes;
	}
	
	public int getId() {
		return idStore;
	}

	public void setId(int id) {
		this.idStore = id;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public String getAdresse() {
		return Adresse;
	}

	public void setAdresse(String adresse) {
		Adresse = adresse;
	}

	public int getTel() {
		return Tel;
	}

	public void setTel(int tel) {
		Tel = tel;
	}

	public Time getHeure_ouverture() {
		return heure_ouverture;
	}

	public void setHeure_ouverture(Time heure_ouverture) {
		this.heure_ouverture = heure_ouverture;
	}

	public Time getHeure_fermeture() {
		return Heure_fermeture;
	}

	public void setHeure_fermeture(Time heure_fermeture) {
		Heure_fermeture = heure_fermeture;
	} 
	
	
}
