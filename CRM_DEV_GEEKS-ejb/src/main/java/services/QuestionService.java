package services;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entities.Question;
import interfaces.IQuestionService;

@Stateless
public class QuestionService implements IQuestionService, Serializable {

	@PersistenceContext(unitName = "crm-ejb")
	EntityManager em;

	@Override
	public int addQuestion(Question emp) {
		if (emp != null) {
			em.persist(emp);
			return (emp.getIdQuestion());
		} else
			return 0;
	}

	@Override
	public void removeQuestion(int id) {

		em.remove(em.find(Question.class, id));

	}

	@Override
	public void QuestionUpdate(Question emp) {
	Question emps=em.find(Question.class,emp.getIdQuestion());
	emps.setIdQuestion(emp.getIdQuestion());
	/* Query query = em.createQuery(  "UPDATE Question q SET q.intitule =:intit WHERE q.idQuestion=:id AND q.idUser:idUser");
	query.setParameter("intit", emp.getIntitule());
	query.setParameter("id", emp.getIdQuestion());
	query.setParameter("idUser", emp.getUser().getId());
	query.executeUpdate();*/
	emps.setIntitule(emp.getIntitule());
	em.merge(emps);
	}

	@Override
	public Question findQuestion(int id) {
		Question emp = em.find(Question.class, id);
		return emp;

	}

	@Override
	public List<Question> findAllQuestion() {

		List<Question> el = em.createQuery("from Question", Question.class).getResultList();
		System.out.println(el);
		return el;
	}

}
