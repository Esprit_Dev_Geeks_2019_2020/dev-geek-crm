package services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entities.Answer;
import interfaces.IAnswerService;
@Stateless
public class AnswerService implements IAnswerService {
	@PersistenceContext(unitName = "crm-ejb")
	EntityManager em;

	@Override
	public int addAnswer(Answer emp) {
		if(emp != null)
		{em.persist(emp);
		return (emp.getIdAnswer());
		}
		else return 0;
	}

	@Override
	public void removeAnswer(int id) {

		em.remove(em.find(Answer.class, id));

	}

	@Override
	public void AnswerUpdate(Answer emp) {

		Answer emps = em.find(Answer.class, emp.getIdAnswer());

		em.merge(emps);
	}

	@Override
	public Answer findAnswer(int id) {
		Answer emp = em.find(Answer.class, id);
		return emp;

	}

	@Override
	public List<Answer> findAllAnswer() {

		List<Answer> el = em.createQuery("from Answer", Answer.class).getResultList();
		System.out.println(el);
		return el;
	}
}
