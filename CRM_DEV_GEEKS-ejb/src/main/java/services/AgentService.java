package services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entities.Agent;
import interfaces.IAgentService;
@Stateless
public class AgentService implements IAgentService {

	@PersistenceContext(unitName = "crm-ejb")
	EntityManager em;
	
	@Override
	public int addAgent(Agent emp) {
		if(emp != null)
		{em.persist(emp);
		return (emp.getIdAgent());
		}
		else return 0;
	}

	@Override
	public void removeAgent(int id) {
		
		em.remove(em.find(Agent.class, id));
		
	}

	@Override
	public void AgentUpdate(Agent emp) {
	
		Agent emps=em.find(Agent.class, emp.getIdAgent());
		
		em.merge(emps);
	}

	
	@Override
	public Agent findAgent(int id) {
			Agent emp=em.find(Agent.class, id);
			return emp;
	 	
	}
	
	@Override
	public List<Agent> findAllAgent() {
	
		List<Agent> el = em.createQuery("from Agent",Agent.class).getResultList();
		System.out.println(el);
		return el;
	}

}
