package services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import entities.Prospection;
import interfaces.IProspectionService;
@Stateless
public class ProspectionService implements IProspectionService {
	@PersistenceContext(unitName = "crm-ejb")
	EntityManager em;
	@Override
	public int addProspection(Prospection emp) {
		if(emp != null)
		{em.persist(emp);
		return (emp.getIdProspection());
		}
		else return 0;
	}

	@Override
	public void removeProspection(int id) {
		
		em.remove(em.find(Prospection.class, id));
		
	}

	@Override
	public void ProspectionUpdate(Prospection emp) {
	
		Prospection emps=em.find(Prospection.class, emp.getIdProspection());
		
		em.merge(emps);
	}

	
	@Override
	public Prospection findProspection(int id) {
			Prospection emp=em.find(Prospection.class, id);
			return emp;
	 	
	}
	
	@Override
	public List<Prospection> findAllProspection() {
	
		List<Prospection> el = em.createQuery("from Prospection",Prospection.class).getResultList();
		System.out.println(el);
		return el;
	}
}
