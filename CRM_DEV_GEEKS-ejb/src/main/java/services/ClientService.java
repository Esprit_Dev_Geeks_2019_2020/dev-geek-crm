package services;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;
import entities.Personne;

@Remote
public interface ClientService {
	public boolean ajouterPersonne(Personne P) ; 
	public boolean modifierPersonne(Personne P) ; 
	public Personne rechercherPersonne(String email) ; 
	public List<Personne> listerPersonnes() ;  
	
	
	
	//boolean authenticate(String login , String password)  ; 
//	void deactivate (Account account) ; 
	//List<Account> getAllAccounts() ; 
}
