package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import entities.Personne;
import entities.Role;

@Stateless
public class ClientServiceImpl implements ClientService,ClientServiceLocale{
	@PersistenceContext	
	EntityManager entityManager ; 
	
	@Override
	public boolean ajouterPersonne(Personne P) {
		// TODO Auto-generated method stub
		Personne ok = rechercherPersonne(P.getEmail()) ;
		//Personne ok = null ; 
		if (ok!=null) {
			// il existe deja !
			return false ; 
		}else {
			System.out.println("\nemail a ajouter : "+P.getNumtel());
			String ch = P.getNumtel().charAt(0)+"" ; 
			if (ch.equals("5"))
				P.setRole(Role.CLIENT);
			else
				P.setRole(Role.PROSPECT);			
			P.setActif(1); /// rendre le client par defaut actif
			entityManager.persist(P); //au lieu de persist
			return true ; 
		}		
	}

	@Override
	public boolean modifierPersonne(Personne P) {
		// TODO Auto-generated method stub
		Personne PersRechercher = rechercherPersonne(P.getEmail()) ;
		if (PersRechercher!=null) {
			P.setIdPersonne(PersRechercher.getIdPersonne());
			P.setRole(PersRechercher.getRole());
			entityManager.merge(P);
			return true ; 
		}
		return false;
	}

	@Override
	public Personne rechercherPersonne(String email) {
		// TODO Auto-generated method stub
		TypedQuery<Personne> query = entityManager.createQuery(
					"select P from Personne P where P.email=:email",Personne.class			
				);
				query.setParameter("email", email) ; 
				
		List<Personne> listPers= query.getResultList() ; 
		if (!listPers.isEmpty())
			return listPers.get(0) ; 
		else
			return null ;
	}

	@Override
	public List<Personne> listerPersonnes() {
		// TODO Auto-generated method stub
		return entityManager.createQuery("SELECT P FROM Personne P",Personne.class)
				.getResultList()		   		
				;
	}

}
